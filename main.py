from telegram import Update
from telegram.ext import (
    filters,
    MessageHandler,
    ApplicationBuilder,
    CommandHandler,
    ContextTypes,
    PicklePersistence,
)

from horde_constants import ANON_API_KEY
from horde_ai_chat_module import *
from common_horde_telegram.common_horde_telegram import token

WELCOME_TEXT = """This an AI chat bot that is powered by AIHorde. Please see https://aihorde.net/ for more information.

This version can only have one AI chat per bot chat and it's trying to use all text as context."""


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=WELCOME_TEXT,
    )


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=WELCOME_TEXT
        + """
/help - this help message (suddenly!)
/clear_history - clear ai bot memory
/history - show raw ai bot history
/models [model1; model2] - set models to be used for ai (empty = any)
/token [token] - show/input your AIHorde token here
         Users with a token and kudos to spend have priority""",
    )


async def history(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=context.chat_data.get("message_history", "No recorded bot history"),
    )


async def clear_history(update: Update, context: ContextTypes.DEFAULT_TYPE):
    del context.chat_data["message_history"]
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="History cleared" + context.chat_data.get("message_history", "."),
    )


def reparse_models(args: list[str]) -> list[str]:
    original_args = " ".join(args)
    return [x for x in original_args.split("; ")]


async def models(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if len(context.args) == 0:
        del(context.chat_data["models"])
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Models reset",
        )
        return
    models: list = reparse_models(context.args)
    context.chat_data["models"] = models
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Changed model(s) to: " + "; ".join(models),
    )


async def text(update: Update, context: ContextTypes.DEFAULT_TYPE):
    message = await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Please wait while I generate your response...",
    )
    message_history = context.chat_data.get("message_history", "")
    message_history += (
        "\n### Instruction:\n" + update.message.text + "\n\n### Response:\n"
    )
    response = generate_text(
        update.message.text,
        api_key=context.user_data.get("token", ANON_API_KEY),
        models=context.chat_data.get("models", []),
    )
    print(json.dumps(response.json(), indent=2))
    generation_id = response.json()["id"]
    try:
        ready = False
        old_text = ""
        while not ready:
            response = status(generation_id)
            print(json.dumps(response.json(), indent=2))
            if (
                response.json()["finished"] == 1
                or response.json()["done"] == True
                or response.json()["faulted"] == True
                or response.json()["is_possible"] == False
            ):
                ready = True
                continue
            if response.json()["waiting"] == 1:
                new_text = (
                    "Please wait while I generate your response...\n"
                    + "Position in queue: "
                    + str(response.json()["queue_position"])
                )
            elif response.json()["processing"] == 1:
                new_text = (
                    "Please wait while I generate your response...\n"
                    + "Processing. ETA: "
                    + str(response.json()["wait_time"])
                    + "s"
                )
            if old_text != new_text:
                old_text = new_text
                await context.bot.edit_message_text(
                    text=new_text,
                    chat_id=update.effective_chat.id,
                    message_id=message.message_id,
                )
            await asyncio.sleep(1)
    except KeyboardInterrupt:
        print("Received Ctrl-C. Deleting generation...")
        delete(generation_id)
        exit(0)
    except Exception as e:
        print(e)
        print("Unexpected error. Deleting generation...")
        delete(generation_id)
        exit(1)
    if response.json()["is_possible"] == False:
        await context.bot.edit_message_text(
            chat_id=update.effective_chat.id,
            message_id=message.message_id,
            text="Not possible to complete.",
        )
        delete(generation_id)
        return
    generation = response.json()["generations"][0]
    text = generation["text"]
    text = clean_text(text)
    context.chat_data["message_history"] = message_history + text + "\n"
    print(text)
    await context.bot.edit_message_text(
        chat_id=update.effective_chat.id,
        message_id=message.message_id,
        text=text
        + "\n\nServed by "
        + generation["worker_name"]
        + "\nModel: "
        + generation["model"],
    )


if __name__ == "__main__":
    try:
        with open("tg_bot_token.txt") as f:
            tg_bot_token = f.read()
    except FileNotFoundError:
        print("Please specify your TG bot token in tg_bot_token.txt")
        exit(1)
    application = (
        ApplicationBuilder()
        .token(tg_bot_token)
        .persistence(
            persistence=PicklePersistence(filepath="persistence_file_aihorde_chatbot")
        )
        .build()
    )

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("help", help))
    application.add_handler(CommandHandler("clear_history", clear_history))
    application.add_handler(CommandHandler("history", history))
    # application.add_handler(CommandHandler("debug", debug))
    application.add_handler(CommandHandler("models", models))
    application.add_handler(CommandHandler("token", token))
    application.add_handler(
        MessageHandler(filters.TEXT & (~filters.COMMAND), text, block=False)
    )

    application.run_polling()
